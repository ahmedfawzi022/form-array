import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { BaseComponent } from '../../general/base/base.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ToasterService } from 'angular2-toaster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { group } from '../../../models/groups/group';
import { GroupService } from '../../../services/group/group.service';
import { DataService } from "../../../services/data/data.service";
import { PreviousRouteService } from '../../../services/base/previous-route.service';
import { TranslateService } from '@ngx-translate/core';
import { NbLayoutDirectionService } from '@nebular/theme';
import { PdfmakeService } from 'ng-pdf-make';
import { UserManagementObject } from '../../../models/share/UserManagemantObject';
import { menuObject } from '../../../models/ACL/menuObject';
import { takeWhile } from 'rxjs/operators/takeWhile';
import { TypesService } from '../../../services/types/types.service';

@Component({
  selector: 'new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.scss']
})
export class NewGroupComponent extends BaseComponent implements OnInit,OnDestroy {
  alive:boolean = true;
  editId: any;
  usersSelected: any;
  // rolesSelected: any
  //Define Form Group with its controls
  NewGroup: FormGroup;
  nameControl: FormControl;
  idControl: FormControl
  descriptionControl: FormControl;
  rolesControl: FormArray;
  groupsRolesControl: FormControl;
  usersControl: FormControl;
  regionId:number;
  generalRegionId :number;
  sectorId :number;
  newGroup: group;
  group: group;
  groups = [];
  template: string = '<img class="custom-spinner-template"  src="assets/images/loading.gif">'
  datasaved = false;
  pleasewait: string;
  userManagementObject = new UserManagementObject();

  menuObject: menuObject = new menuObject();

  constructor(private groupService: GroupService,private typesService: TypesService, public spinnerService: Ng4LoadingSpinnerService,
    private route: ActivatedRoute, private router: Router, public data: DataService, public toasterService: ToasterService
    , public modalService: NgbModal, public previousRouteService: PreviousRouteService, public translate: TranslateService
    , public directionService: NbLayoutDirectionService, public pdfmake: PdfmakeService) {
    super(modalService, toasterService, previousRouteService, translate, directionService, data, pdfmake,spinnerService);

  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.editId = params['id']
    });

    // create form group and its form controls and
    this.createFormControls();
    this.createForm();

     this.getUsers();


    if (this.editId > 0)
      {this.editGroup();}
    else
      {this.getRoles();}

    this.translateGroups();
    this.translate.onLangChange
    .pipe(takeWhile(() => this.alive))
    .subscribe(
      message => {
        this.translateGroups();
        //this.Translate()
      });
  }
  regionChange(event){
    this.regionId = event;
    this.getUsers();
  }

  generalRegionChange(event){
    this.generalRegionId = event;
  }
  sectorChange(event){
    this.sectorId = event;
  }

  // bindRolesControl() {
  //   // const control = <FormArray>this.rolesControl;
  //   // this.rolesControl.controls = [];
  //   // this.groups.forEach(x => {
  //   //   x.roles.forEach(element => {
  //   //     control.push(new FormControl(element.isChecked));
  //   //   });

  //   // })

  //   this.groupsRolesControl.setValue(this.groups);
  // }

  getRoles() {

    this.groupService.getGroupsRoles()
      .subscribe(
        data => {
          // const model = JSON.parse(data._body).data;
          this.groups = data.data;
         // this.bindRolesControl();
          //this.roles = this.newGroup.roles;
          this.groupsRolesControl.setValue(this.groups);
          this.spinnerService.show();
          // if (this.editId > 0) {
          //   this.editGroup();
          // }
          this.spinnerService.hide();

        },
        error => {
          this.spinnerService.hide();
          try {
            if (error.error.errorCode)
              this.openModal(error.error.errorCode, error.error.errorMessage);
            else
              this.openModal(this.userManagementObject.error, this.userManagementObject.errorMessage);
          } catch (err) {
            this.openModal(this.userManagementObject.error, this.userManagementObject.errorMessage);
          }
        },
      );

  
  }
  bindSelectAllRoles(){
    this.groups.forEach(element => {
        // var unselectedRoles = element.roles.filter(x => x.isChecked == false);
        // if (unselectedRoles.length == 0)
        //   element.isChecked = true;
        // else
        //   element.isChecked = false;
        this.checkGroupSelectAll(element);
    });
  }
  translateGroups() {

    this.translate.get([
      'userManagement.errorMessage',
      'userManagement.error',
      'operations.pleasewait',
      'userManagement.groupSuccessfullMessage'
    ], {})
    .pipe(takeWhile(() => this.alive))
    .subscribe(translation => {
      this.userManagementObject.errorMessage = translation['userManagement.errorMessage'];
      this.userManagementObject.error = translation['userManagement.error'];
      this.pleasewait = translation['operations.pleasewait'];
      this.userManagementObject.groupSuccessfullMessage = translation['userManagement.groupSuccessfullMessage'];
    });
  }
  ngOnDestroy() {
    this.alive = false;
  }

  editGroup() {

    this.groupService.getGroupById(this.editId)
      .subscribe(
        data => {
          this.spinnerService.hide();
          this.group = data.data;
          this.groups = data.data.groups;
          if (this.editId) {
            this.idControl.setValue(this.group.id);

          }
          this.nameControl.setValue(this.group.name);
          this.descriptionControl.setValue(this.group.description);
          this.usersControl.setValue(this.group.users);
          //this.bindRolesControl();
          this.bindSelectAllRoles();
          
          this.groupsRolesControl.setValue(this.groups);
          //this.rolesControl.setValue(this.group.roles);
        },
        error => {
          this.spinnerService.hide();
          try {
            if (error.error.errorCode)
              this.openModal(error.error.errorCode, error.error.errorMessage);
            else
              this.openModal(this.userManagementObject.error, this.userManagementObject.errorMessage);
          } catch (err) {
            this.openModal(this.userManagementObject.error, this.userManagementObject.errorMessage);
          }
        },
      );
  }

  // create form controls that we need to validate with their needed validators
  createFormControls() {

    this.nameControl = new FormControl(null, [
      Validators.required,
    ]),
      this.idControl = new FormControl(0, [
      ]),
      this.descriptionControl = new FormControl(null, [
        // Validators.required,
      ]),
      this.rolesControl = new FormArray([]),
      this.groupsRolesControl = new FormControl(),
      this.usersControl = new FormControl(null, [
        // Validators.required,
      ])
  }

  // add form controls to form group
  createForm() {
    this.NewGroup = new FormGroup({
      name: this.nameControl,
      id: this.idControl,
      description: this.descriptionControl,
      roles: this.rolesControl,
      groupsRoles: this.groupsRolesControl,
      users: this.usersControl
    });
  }

  getUsers() {
   this.typesService.GetUsers(this.sectorId, this.generalRegionId, this.regionId)
      .subscribe(
        data => {
          // const model = JSON.parse(data._body).data;
          this.newGroup = data.data;
          //this.roles = this.newGroup.roles;
          this.spinnerService.show();
          // if (this.editId > 0) {
          //   this.editGroup();
          // }
          this.spinnerService.hide();

        },
        error => {
          this.spinnerService.hide();
          try {
            if (error.error.errorCode)
              this.openModal(error.error.errorCode, error.error.errorMessage);
            else
              this.openModal(this.userManagementObject.error, this.userManagementObject.errorMessage);
          } catch (err) {
            this.openModal(this.userManagementObject.error, this.userManagementObject.errorMessage);
          }
        },
      );
  }


  checkGroupSelectAll(element){
    var unselectedRoles = element.roles.filter(x => x.isChecked == false);
    if (unselectedRoles.length == 0)
      element.isChecked = true;
    else
      element.isChecked = false;
  }

  onChangeRoles(roleId, groupId, event) {
    let isChecked = event.target.checked;
    let groupIndex = this.groups.findIndex(x => x.id == groupId);
    let index = this.groups[groupIndex].roles.findIndex(x => x.id == roleId);
    this.groups[groupIndex].roles[index].isChecked = isChecked;
    this.checkGroupSelectAll(this.groups[groupIndex]);
    this.groupsRolesControl.setValue(this.groups);

    
  }

  onChangeRolesAll(groupId, event){
    let isChecked = event.target.checked;
    let groupIndex = this.groups.findIndex(x => x.id == groupId);
    this.groups[groupIndex].isChecked = isChecked;
    this.groups[groupIndex].roles.forEach(element => {
      element.isChecked = isChecked;
    }); 
    this.groupsRolesControl.setValue(this.groups);
  }
  NewGroupFunc() {
    this.spinnerService.show();

    this.groupService.addNewGroup(this.NewGroup.value)
      .subscribe(
        data => {
          this.router.navigateByUrl(`/pages/user-management/view-group`);
          // this.spinnerService.show();
          this.makeToast(this.userManagementObject.groupSuccessfullMessage);
          this.NewGroup.reset();
          this.data.changeMessage(this.datasaved)
          this.spinnerService.hide();

        },
        error => {
          this.spinnerService.hide();
          try {
            if (error.error.errorCode)
              this.openModal(error.error.errorCode, error.error.errorMessage);
            else
              this.openModal(this.userManagementObject.error, this.userManagementObject.errorMessage);
          } catch (err) {
            this.openModal(this.userManagementObject.error, this.userManagementObject.errorMessage);
          }
        },
      );
  }

}
